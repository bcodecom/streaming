package com.example.streaming;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;

import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;

public class MainActivity extends AppCompatActivity implements OnPreparedListener {

    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupVideoView();
    }

    private void setupVideoView() {

        // Make sure to use the correct VideoView import
        videoView = (VideoView)findViewById(R.id.video_view);
        videoView.setOnPreparedListener(this);

        //videoView.setVideoPath("/Users/administrador/Documents/proyects/Streaming/app/src/main/res/raw/morgana.mp4");

        String path = "android.resource://"+ getPackageName().toString() +"/"+R.raw.morgana;
        Uri u = Uri.parse(path);

        videoView.setVideoURI(u);

        //videoView.setVideoURI(Uri.parse("https://archive.org/download/Popeye_forPresident/Popeye_forPresident_512kb.mp4"));
    }


    @Override
    public void onPrepared() {
        videoView.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        videoView.stopPlayback();
    }
}
